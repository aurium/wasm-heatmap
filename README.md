WAsm Heatmap
============

Creates a heatmap layer for Leaflet.js and is potentially generic, for other tile based mapping solutions.

This works with WebWorkers to prevent UI locking and a tiles drawer engine written in Go and compiled to WebAssembly to be as fast as native code.

**This is a working in progress. It is not done yet. 🔨**

Try it now: https://aurium.gitlab.io/wasm-heatmap


Building
--------

This project has no node.js dependencies, however as it is planed to become a package, there is a `package.json` file with some configured commands to help the development and usage. So, it will be good if you have node.js installed and the `npm` command in your path.

To build, simply run:
```bash
$ npm run build
```

You may want to hack and you need to re-build on each change, so run:
```bash
$ npm run build:watch
```

Ouch! You don't have golang in the correct version. You must have docker:
```bash
$ DOCKERIZED=true npm run build:watch
```

You will hack offline? In a plane or in a farm, the development mode downloads the assets and store that on dist/assets:
```bash
$ TARGET_ENV=development DOCKERIZED=true npm run build:watch
```


Running
-------

There is a script `tools/server` to help you in local testing. Just run:
```bash
$ npm run start
```

If you need to use another port, run:
```bash
$ npm run start 8080
```


Deploy
------

I believe it is not be deployed as it is, but to be used in other projects, however you can just push the files inside `dist` to a web server and *vu'a la*.


TODO
----

* [x] WebWorker out the box to prevent UI locking.
* [x] WebAssembly tiles drawer to be faster then a JS implementation.
* [ ] Manange N workers to speed up tile drawing with more threads.


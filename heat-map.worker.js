/*\
 *  This WebWorker is only a middleware between the HeatmapLayer Class
 *  and the WebAssembly that really does the job.
\*/

const UNDEFSTR = 'undefined';
const go = new Go();
var wasmLoaded = false;
var cmdQueue = [];

WebAssembly
.instantiateStreaming(fetch('heat-map.wasm?v=%BUILD_DATE%'), go.importObject)
.then(result => {
    debug('Heatmap WAsm loaded! Running...');
    go.run(result.instance)
    .then(result => log('WA exited.', result))
    .catch(err => log('WA Main Fail:', err));
    setTimeout(()=>{ wasmLoaded = true }, 10);
    walkCmdQueue();
});

var middlewares = {};

class HeatmapMiddleware {

    constructor(id, config) {
        this.id = id;
        this.tileSize = config.tileSize;
        middlewares[id] = this;
        this.debug(`Heatmap worker middleware "${id}" was created with tile size ${this.tileSize}.`);
        this.zooming = false;
        this.cmd_setZoom(config.zoom);
        this.cmd_setRadius(config.radius);
        this.cmd_setMaxIntensity(config.maxIntensity);
        this.cmd_setGradient(config.gradient);
        this.notifyIsInitialized();
    }

    isInitialized() {
        return wasmLoaded && this._gradientSeted;
    }

    canDraw() {
        return this.isInitialized() && !this.zooming;
    }

    notifyIsInitialized() {
        if (this.isInitialized()) this.send('isInitialized');
        else setTimeout(this.notifyIsInitialized.bind(this), 50);
    }

    debug(message) { this.send('debug', message) }
    error(message) { this.send('error', message) }
    info(message)  { this.send('info',  message) }

    send(command, data) {
        postMessage([this.id, command, data]);
    }

    onMessage(command, data) {
        if (this['cmd_'+command]) this['cmd_'+command](data);
        else this.error(`The heatmap worker middleware command "${command}" does not exists.`);
    }

    cmd_addPoints(points, retryDelay=50) {
        this.debug(`Adding ${points.length} points...`);
        let heater = addPoints(this.id, points);
        this.send('pointsAdded', {pointsLength: points.length, heater});
    }

    cmd_setGradient(gradient, retryDelay=50) {
        setGradient(this.id, gradient);
        this._gradientSeted = true;
        this.send('gradientSeted', gradient.length);
    }

    cmd_setMaxIntensity(maxIntensity, retryDelay=50) {
        setMaxIntensity(this.id, maxIntensity)
    }

    cmd_setRadius(radius, retryDelay=50) {
        setRadius(this.id, radius)
    }

    cmd_zoomStart() {
        this.debug(`Zooming start.`);
        this.zooming = true;
    }

    cmd_setZoom(zoom, retryDelay=50) {
        this.debug(`Update zoom: ${zoom}.`)
        this.zooming = false;
        this.zoom = setZoom(this.id, zoom);
    }

    cmd_createTile({x, y}, retryDelay=250) {
        if (this.canDraw()) {
            this.send('createTileStarted');
            this.debug(`Creating tile(${this.tileSize}) at ${x},${y}. Zoom: ${this.zoom}`);
            var tile = createTile(this.id, x, y, this.tileSize);
            this.send('tile', {x, y, z: this.zoom, base64: tile});
        } else {
            this.debug(`createTile() is waiting heatmap draw configuration...`);
            let callback = this.cmd_createTile.bind(this, {x, y}, retryDelay*2);
            setTimeout(callback, retryDelay);
        }
    }

}

function panicNotification(message, stack) {
    postMessage([0, 'panic', {message, stack:stack.split('\n')}]);
}

function debug(message) {
    postMessage([0, 'debug', message]);
}

onmessage = (ev)=> {
    if (!ev || !ev.data) {
        return postMessage([0, 'error', 'Bad event from host', ev.constructor.name]);
    }

    var id = ev.data[0] || UNDEFSTR;
    if (id === '0') id = UNDEFSTR;
    var cmd = ev.data[1] || UNDEFSTR;
    var data = ev.data[2] || {};

    if (id === UNDEFSTR) {
        postMessage([0, 'error', `The Heatmap id is undefined for "${cmd}".`]);
    }
    else {
        cmdQueue.push([id, cmd, data]);
        if (wasmLoaded) walkCmdQueue();
    }
}

function walkCmdQueue() {
    while (cmdQueue.length > 0) {
        let [id, cmd, data] = cmdQueue.shift();
        if (cmd === 'init') {
            new HeatmapMiddleware(id, data);
        } else {
            if (middlewares[id]) middlewares[id].onMessage(cmd, data);
            else console.error(`There is no heatmap worker middleware with id "${id}" to "${cmd}".`);
        }
    }
}

debug('Heatmap Worker loaded');
postMessage([0, 'workerLoaded']);

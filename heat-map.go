package main

import "fmt"
import "math"
import "syscall/js"
import "image"
import "image/png"
import "bytes"
import "encoding/base64"
import "runtime"
import "strings"

var jsUint8ClampedArray = js.Global().Get("Uint8ClampedArray")
var jsImageData = js.Global().Get("ImageData")

var memoRadius       = make(map[string]int)
var memoHeater       = make(map[string]float64)
var memoMaxIntensity = make(map[string]float64)
var memoZoom         = make(map[string]float64)
var memoPoints       = make(map[string][][3]float64)
var memoGradients    = make(map[string][][3]uint8)

func panicNotifyer() {
    if err := recover(); err != nil {
        var pc [16]uintptr
        var stack = make([]string, 0)
        var stackFunc, stackFile string
        var stackLine int
        n := runtime.Callers(2, pc[:])
        for _, pc := range pc[:n] {
            fn := runtime.FuncForPC(pc)
            if fn == nil { continue }
            stackFile, stackLine = fn.FileLine(pc)
            stackFunc = fn.Name()
            if strings.HasPrefix(stackFunc, "runtime.") { continue }
            stack = append(stack, fmt.Sprintf("%s\t%s:%d", stackFunc, stackFile, stackLine))
        }
        js.Global().Call(
            "panicNotification",
            fmt.Sprintf("%v", err),
            strings.Join(stack, "\n"))
    }
}

func addPoints(this js.Value, args []js.Value) interface{} {
    defer panicNotifyer()
    var myPoints [][3]float64
    id := args[0].String()
    newPoints := args[1]
    heater := float64(0)
    myPoints, present := memoPoints[id]
    if !present {
        myPoints := make([][3]float64, 0)
        memoPoints[id] = myPoints
    }
    for i:=0; i<newPoints.Length(); i++ {
        p      := newPoints.Index(i)
        lat    := p.Index(0).Float()
        lng    := p.Index(1).Float()
        weight := p.Index(2).Float()
        heater = math.Max(heater, weight)
        myPoints = append(myPoints, [3]float64{lat, lng, weight})
    }
    memoPoints[id] = myPoints
    memoHeater[id] = heater
    return heater
}

func setRadius(this js.Value, args []js.Value) interface{} {
    defer panicNotifyer()
    id := args[0].String()
    memoRadius[id] = args[1].Int()
    return memoRadius[id]
}

func setZoom(this js.Value, args []js.Value) interface{} {
    defer panicNotifyer()
    id := args[0].String()
    memoZoom[id] = args[1].Float()
    return memoZoom[id]
}

func setMaxIntensity(this js.Value, args []js.Value) interface{} {
    defer panicNotifyer()
    id := args[0].String()
    memoMaxIntensity[id] = args[1].Float()
    return memoMaxIntensity[id]
}

func dist(p1, p2 [3]float64, zoom float64) float64 {
    defer panicNotifyer()
    // TODO: we need memoSize
    x1, y1, _ := pointToPix(p1, 0, 0, 256, zoom)
    x2, y2, _ := pointToPix(p2, 0, 0, 256, zoom)
    return math.Sqrt( float64( (x1-x2)^2 + (y1-y2)^2 ) )
}

func setGradient(this js.Value, args []js.Value) interface{} {
    defer panicNotifyer()
    id := args[0].String()
    newGradient := args[1]
    myGradient := make([][3]uint8, 0)
    for i:=0; i<newGradient.Length(); i++ {
        color := newGradient.Index(i)
        red   := uint8(color.Index(0).Int())
        green := uint8(color.Index(1).Int())
        blue  := uint8(color.Index(2).Int())
        myGradient = append(myGradient, [3]uint8{red, green, blue})
    }
    memoGradients[id] = myGradient
    return newGradient.Length()
}

// js: function createTile(id, tileX, tileY, tileSize)
func createTile(this js.Value, args []js.Value) interface{} {
    defer panicNotifyer()
    id           := args[0].String()
    tileX        := args[1].Int()
    tileY        := args[2].Int()
    size         := args[3].Int()
    radius       := memoRadius[id]
    zoom         := memoZoom[id]
    gradient     := memoGradients[id]
    points       := memoPoints[id]
    numPoints    := len(points)
    heater       := memoHeater[id]
    maxIntensity := memoMaxIntensity[id]
    if maxIntensity == -1 { maxIntensity = heater }
    heatMatrix := make([][]float64, size) // Sum the weight of each pixel.
    for i:=0; i<size; i++ { heatMatrix[i] = make([]float64, size) }
    for i:=0; i<numPoints; i++ {
        x, y, weight := pointToPix(points[i], tileX, tileY, size, zoom)
        plotHeat(heatMatrix, size, x, y, radius, weight)
    }
    tile := image.NewNRGBA(image.Rect(0, 0, size, size))
    for y:=0; y<size; y++ {
        for x:=0; x<size; x++ {
            pos := tile.PixOffset(x, y)
            r,g,b,a := heatToColor(heatMatrix[y][x], maxIntensity, gradient)
            tile.Pix[pos+0] = r
            tile.Pix[pos+1] = g
            tile.Pix[pos+2] = b
            tile.Pix[pos+3] = a
        }
    }
    var pngBuffer bytes.Buffer
    png.Encode(&pngBuffer, tile)
    return base64.StdEncoding.EncodeToString(pngBuffer.Bytes())
}

func heatToColor(heat, maxIntensity float64, gradient [][3]uint8) (uint8, uint8, uint8, uint8) {
    var r, g, b, a uint8
    gradSteps := len(gradient)
    stepLen := maxIntensity / float64(gradSteps)
    gradStepF, gradPos := math.Modf(heat / stepLen)
    gradStep := int(math.Round(gradStepF))
    if (gradStep >= gradSteps) {
        r = gradient[gradSteps-1][0]
        g = gradient[gradSteps-1][1]
        b = gradient[gradSteps-1][2]
        a = 255
    } else {
        if (gradStep == 0) {
            r = gradient[0][0]
            g = gradient[0][1]
            b = gradient[0][2]
            a = uint8(math.Round(255*gradPos))
        } else {
            gradPosInv := 1 - gradPos
            r = uint8(math.Round(float64(gradient[gradStep-1][0])*gradPosInv +
                                 float64(gradient[gradStep-0][0])*gradPos))
            g = uint8(math.Round(float64(gradient[gradStep-1][1])*gradPosInv +
                                 float64(gradient[gradStep-0][1])*gradPos))
            b = uint8(math.Round(float64(gradient[gradStep-1][2])*gradPosInv +
                                 float64(gradient[gradStep-0][2])*gradPos))
            a = 255
        }
    }
    return r, g, b, a
}

func pointToPix(point [3]float64, tileX, tileY, size int, zoom float64) (int, int, float64) {
    defer panicNotifyer()
    lat := point[0]
    lng := point[1]
    weight := point[2]
    x := (lng + 180.0) / 360.0 * math.Exp2(zoom)
    y := (1.0 - math.Log(
             math.Tan(lat*math.Pi/180.0) +
             1.0 / math.Cos(lat*math.Pi/180.0) ) /
         math.Pi) /
         2.0 * (math.Exp2(zoom))
    relativeX := int( (x-float64(tileX)) * float64(size))
    relativeY := int( (y-float64(tileY)) * float64(size))
    return relativeX, relativeY, weight
}

func plotHeat(heatMatrix [][]float64, size, pointX, pointY, radius int, weight float64) {
    defer panicNotifyer()
    for x:=pointX-radius; x<pointX+radius; x++ {
        if x >= 0 && x < size {
            for y:=pointY-radius; y<pointY+radius; y++ {
                if y >= 0 && y < size {
                    dX := pointX - x
                    dY := pointY - y
                    dist := math.Sqrt(float64(dX*dX + dY*dY))
                    force := 1.0 - (dist / float64(radius))
                    if force < 0 { force = 0 }
                    heatMatrix[y][x] += weight * force
                }
            }
        }
    }
}

func main() {
    c := make(chan struct{}, 0) // impede que o programa feche

    js.Global().Set("addPoints", js.FuncOf(addPoints))
    js.Global().Set("setGradient", js.FuncOf(setGradient))
    js.Global().Set("setMaxIntensity", js.FuncOf(setMaxIntensity))
    js.Global().Set("createTile", js.FuncOf(createTile))
    js.Global().Set("setRadius", js.FuncOf(setRadius))
    js.Global().Set("setZoom", js.FuncOf(setZoom))

    <-c // impede que o programa feche
}


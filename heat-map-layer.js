"use strict";

var DEBUG = true;
const UNDEFSTR = 'undefined';
const isMovingAtt = '__heatmap_helper__isMoving';
const defaultGradient = ['0AF', '0F0', 'FF0', 'F00'];

const mkId = (prefix)=> prefix +'-'+ Math.random().toString(36).split('.')[1]
const debug = (...args)=> DEBUG && console.log(...args);

var heatmapWorker = new Worker('heat-map.worker.js?v=%BUILD_DATE%');
var workerLoaded = false;

heatmapWorker.onmessage = (ev)=> {
    if (!ev || !ev.data) return console.error('Bad event from worker', ev);
    var id = ev.data[0]===0 ? 0 : ev.data[0] || UNDEFSTR;
    var cmd = ev.data[1] || UNDEFSTR;
    var data = ev.data[2] || {};

    if (heatmapLayers[id]) heatmapLayers[id]._getWorkerCmd(cmd, data);
    else if (id === 0) {
        if (cmd === 'workerLoaded') {
            debug('heatmapWorker was loaded!');
            workerLoaded = true;
        }
        else if (cmd === 'debug') {
            debug('heatmapWorker:', data);
        }
        else if (cmd === 'error') {
            console.error('WORKER ERROR!', error);
            Object.values(heatmapLayers).forEach(l => l.cmd_error(error.message||error))
        }
        else {
            Object.values(heatmapLayers)
                  .forEach(layer => layer._getWorkerCmd(cmd, data));
        }
    }
    else throw Error(`There is no HeatmapLayer with id "${id}". {cmd: ${cmd}, data:${JSON.stringify(data)} }`);
};

heatmapWorker.onerror = (error)=> {
    console.error('WORKER ERROR!', error);
    Object.values(heatmapLayers).forEach(l => l.cmd_error(error.message||error))
}

var heatmapLayers = {};

var HeatmapLayer = L.GridLayer.extend({

    initialize(options) {
        this._heatId = mkId('heatLayer');
        this.tiles = {};
        this._heater = 0;
        L.Util.setOptions(this, {
            radius: 10,
            maxIntensity: 0,
            gradient: defaultGradient,
            ...options
        });
        this.setGradient(this.options.gradient, false);
        this.setMaxIntensity(this.options.maxIntensity, false);
        heatmapLayers[this._heatId] = this;
        const tileSize = this.getTileSize()
        if (tileSize.x !== tileSize.y) throw Error('Tiles must be sqares.')
        this._sendCmd('init', { ...this.options, tileSize: tileSize.x });
    },

    /* Define the heat gradient to the map.
     * The parameter `gradient` is an array of collors.
     * Where the colors can be on this formats:
     * `"#<red: one char hex><green: one char hex><blue: one char hex>"`
     * `"#<red: two char hex><green: two char hex><blue: two char hex>"`
     * `[<red: int>, <green: int>, <blue: int>]`
     * (The `#` is optional.)
     */
    setGradient(gradient, updateTiles=true) {
        gradient = gradient.map(color => {
            if (color.toString().match(/^#?[0-9a-f]{3}$/i)) {
                color = color.toString()
                             .replace(/^#?(.)(.)(.)$/, '$1$1$2$2$3$3');
            }
            if (typeof(color) === 'string') {
                if (color.match(/^#?[0-9a-f]{6}$/i)) {
                    color = color.match(/^#?(..)(..)(..)$/)
                                 .slice(1).map(n=>parseInt(n, 16));
                } else {
                    throw Error(`Invalid color format (${color}).`);
                }
            }
            else if (color instanceof Array) {
                if (!(color.length &&
                      isUint8(color[0]) && isUint8(color[1]) && isUint8(color[2]))) {
                    throw Error(`Invalid color format (${JSON.stringify(color)}).`);
                }
            }
            else {
                throw Error(`Invalid color object (${JSON.stringify(color)}).`);
            }
            return color;
        });
        this.options.gradient = gradient;
        if (updateTiles) {
            this._sendCmd('setGradient', gradient);
            this.updateTiles();
        }
    },

    setRadius(radius, updateTiles=true) {
        this.options.radius = radius;
        if (updateTiles) {
            this._sendCmd('setRadius', radius);
            this.updateTiles();
        }
    },

    setMaxIntensity(maxIntensity, updateTiles=true) {
        if (maxIntensity <= 0) maxIntensity = -1;
        this.options.maxIntensity = maxIntensity;
        if (updateTiles) {
            this._sendCmd('setMaxIntensity', maxIntensity);
            this.updateTiles();
        }
    },

    updateZoom(ev) {
        if (!ev) return this.cmd_error('updateZoom was called without event parameter.');
        if (ev.type === 'zoomstart') return this._sendCmd('zoomStart');
        if (ev.type === 'zoomend') return this._sendCmd('setZoom', this._map._zoom);
        this.cmd_error('updateZoom was called with unknown event parameter.');
    },

    // Redraw all tiles;
    updateTiles() {
        this.cmd_debug('Updating tiles...');
        Object.keys(this.tiles).forEach(id =>
            this._sendCmd('createTile', idToCoords(id))
        );
    },

    /* Add heat points to the map.
     * The parameter `points` is an array of point descriptions.
     * Where the point description can be on this formats:
     * `[ <lat:float>, <lng:float> ]`
     * `[ <lat:float>, <lng:float>, <weight:float> ]`
     * `{ lat:<float>, lng:<float> }`
     * `{ lat:<float>, lng:<float>, weight:<float> }`
     * If `weight` is undefined, it will be set to `1`.
     */
    addPoints(points) {
        points = points.map(point => {
            if (point instanceof Array) {
                if (point.length > 2) return point.slice(0,3);
                else return [...point, 1]; // set default weight
            } else {
                let {lat, lng, weight} = point;
                return [lat||0, lng||0, weight||1];
            }
        });
        this._sendCmd('addPoints', points);
    },

    _sendCmd(command, data, retryCount=0) {
        if (retryCount > 50)
            return this.cmd_error(Error(`Command ${command} timeout.`));
        if (workerLoaded) {
            heatmapWorker.postMessage([this._heatId, command, data]);
        }
        else
            setTimeout(this._sendCmd.bind(this, command, data, ++retryCount), 300);
    },

    _getWorkerCmd(command, data) {
        if (this['cmd_'+command]) this['cmd_'+command](data);
        else this.cmd_error(`The HeatmapLayer command "${command}" does not exists.`);
    },

    cmd_debug(message) {
        debug(`DEBUG ${this._heatId}\n`, message);
        this.fireEvent('heatmap:debug', {message, kind: 'DEBUG'});
        this._map.fireEvent('heatmap:debug', {message, kind: 'DEBUG'});
    },
    cmd_info(message) {
        console.log(`${this._heatId}\n${message}`);
        this.fireEvent('heatmap:info', {message, kind: 'INFO'});
        this._map.fireEvent('heatmap:info', {message, kind: 'INFO'});
    },
    cmd_error(message) {
        var stack = ['No stack trace.'];
        console.error(`ERROR ${this._heatId}\n${message}`);
        this.fireEvent('heatmap:error', {message, stack, kind: 'ERROR'});
        this._map.fireEvent('heatmap:error', {message, stack, kind: 'ERROR'});
    },
    cmd_panic({message, stack}) {
        if (!stack) stack = ['No stack trace.'];
        stack.reverse();
        console.error(`WebAssembly PANIC\n${message}\nStack:` +
                      `\n↳ ${stack.join('\n↳ ')}`);
        message = `WebAssembly PANIC: ${message}`;
        this.fireEvent('heatmap:error', {message, stack, kind: 'PANIC'});
        this._map.fireEvent('heatmap:error', {message, stack, kind: 'PANIC'});
    },

    cmd_isInitialized() {
        this.cmd_debug(`HeatmapLayer ${this._heatId} initialized.`);
        this.fireEvent('heatmap:initialized', {layer: this._heatId});
        this._map.fireEvent('heatmap:initialized', {layer: this._heatId});
    },

    cmd_pointsAdded({pointsLength, heater}) {
        this.cmd_debug(`${pointsLength} points added. The heater pixel is ${heater}.`);
        this._heater = heater;
        if (this.__firstCreateTitleCalled) this.updateTiles();
    },

    cmd_gradientSeted(length) {
        this.cmd_debug(`Gradient updated with ${length} color steps.`);
    },

    cmd_tile(tileInfo) {
        var key = coordsToId(tileInfo)
        let tile = this.tiles[key];
        if (!tile) {
            return this.cmd_debug(`The tile ${key} was removed before drawed?`);
        }
        tile.src = 'data:image/png;base64,' + tileInfo.base64;
    },

    cmd_createTileStarted() {
        if (!this.__firstCreateTitleCalled) {
            this.cmd_debug(`First createTile just started for layer ${this._heatId}.`);
        }
        this.__firstCreateTitleCalled = true;
    },

    createTile(coords, done) {
        this._sendCmd('createTile', coords);
        let tile = L.DomUtil.create('img', 'heat-tile');
        tile.alt = '';
        tile.setAttribute('role', 'presentation');
        tile.addEventListener('load', ()=> done());
        tile.addEventListener('error', (err)=> done(err));
        this.tiles[coordsToId(coords)] = tile;
        return tile;
    },

    _removeTile: function (key) {
        delete this.tiles[key];
        return L.GridLayer.prototype._removeTile.call(this, key);
    }
})

export default HeatmapLayer;

export function addHeatmapLayer(map, options={}) {
    options = { ...options, addStyle: true };
    var tilePane = map.getPanes().tilePane;
    var paneName = mkId('heat');
    var heatPane = map.createPane(paneName);
    tilePane.after(heatPane);
    heatPane.classList.add('leaflet-heat-pane');

    let heatmap = new HeatmapLayer({ ...options, pane: paneName, zoom: map._zoom });
    heatmap.addTo(map);

    map.addEventListener('zoomstart', ev => heatmap.updateZoom(ev));
    map.addEventListener('zoomend',   ev => heatmap.updateZoom(ev));

    map.addEventListener('movestart', ev => map[isMovingAtt] = true);
    map.addEventListener('moveend',   ev => map[isMovingAtt] = false);

    if (options.addStyle) {
      tilePane.style.filter = 'grayscale(.8)';
      heatPane.style.mixBlendMode = 'multiply';
    }

    return heatmap;
}

function coordsToId(coords) {
  return `${coords.x}:${coords.y}:${coords.z}`;
}

function idToCoords(id) {
  id = id.match(/(.*):(.*):(.*)/);
  return { x: parseInt(id[1]), y: parseInt(id[2]), z: parseFloat(id[3]) };
}

function isUint8(num) {
  return (typeof(num) == 'number') && 0 <= num && num >= 255;
}
